class Board
  attr_reader :grid

  def initialize(grid = nil)
    if grid == nil
      @grid = [
        [nil, nil, nil],
        [nil, nil, nil],
        [nil, nil, nil]
      ]
    else
      @grid = grid
    end
  end

  def place_mark(position, mark)
    raise "Position occupied" unless empty?(position)
    self[position] = mark
  end

  def empty?(position)
    self[position] == nil
  end

  def winner
    0.upto(2) do |x|
      if @grid[x][0] == @grid[x][1] && @grid[x][1] == @grid[x][2]
        return @grid[x][0] unless @grid[x][0] == nil
      end
    end
    0.upto(2) do |y|
      if @grid[0][y] == @grid[1][y] && @grid[1][y] == @grid[2][y]
        return @grid[0][y] unless @grid[0][y] == nil
      end
    end
    if @grid[0][0] == @grid[1][1] && @grid[1][1] == @grid[2][2]
      return @grid[0][0] unless @grid[0][0] == nil
    end
    if @grid[0][2] == @grid[1][1] && @grid[1][1] == @grid[2][0]
      return @grid[1][1] unless @grid[1][1] == nil
    end
  end

  def over?
    winner || @grid.none? { |pos| pos.any?(&:nil?) }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
