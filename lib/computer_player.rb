class ComputerPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
    @board = []
  end

  def display(board)
    @board = board
  end

  def get_move
    0.upto(2) do |row|
      0.upto(2) do |col|
        next unless @board.empty?([row, col])
        @board.place_mark([row, col], @mark)
        return [row, col] if @board.winner
        @board[[row, col]] = nil
      end
    end

    row = rand(0..2)
    col = rand(0..2)
    [row, col]
  end
end
