class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    print "Please enter where you'd like to make a move: "
    input = gets.chomp
    move = input.match(/^\(?(\d+),?\s*(\d+)\)?/)

    [move[1].to_i, move[2].to_i]
  end

  def display(board)
    board.grid.each do |row|
      row.each do |elem|
        elem == nil ? print(" .") : print(" #{elem.to_s}")
      end
      puts
    end
  end
end
