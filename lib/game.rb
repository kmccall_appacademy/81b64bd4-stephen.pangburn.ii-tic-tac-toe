require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board

  def initialize(player_one, player_two)
    @board = Board.new
    @player1 = player_one
    @player2 = player_two
    @current = player_one
  end

  def current_player
    @current
  end

  def switch_players!
    @current == @player1 ? @current = @player2 : @current = @player1
  end

  def play_turn
    current_player.display
    @board.place_mark(current_player.get_move, current_player.mark)

    switch_players!
  end
end
